# About

## Question Answering API

This is a simple RESTful API written in CodeIgnighter v3.1.9.

You can create/retreive Products and create/retreive Question/Answer data per Product.

## Assessment Brief in detail

Upon completion of this assessment, you will be building a web service in PHP. While the data that you will expose can be anything of your choosing, it is suggested that your output matches the format expected by your mobile app from the mobile web applications module, so that you can hook both applications together if you wish.

The application will contain the following:

• An Object-Oriented design pattern, using at least two classes which work together.

• A backend database from which your output will be produced.

• A Web API: Your service will expose a RESTful API which can be called over HTTP.

• At least two API endpoints, i.e. two types of return object.

• JSON output: Your service does not need to return XML data, JSON will be sufficient.

You can make your web service about anything that interests you. For example, you might want to provide data that can be consumed by your mobile app from Assignment 1 in the Mobile Development module, or something entirely different.

Files should be ordered in a sensible hierarchy and zipped together into a single file for submission, and please follow the correct naming conventions. All code files should be correctly labeled, and reused code (e.g. JavaScript, jQuery, etc.) must state the source of the material deployed, using inline comments.

There are no marks going for front end presentation, however, it is essential that:

• Your outputted JSON is well formed.

• Your endpoint naming conforms to best practices

• Your PHP code is fully OO.

• Your PHP code is clear, readable and contains adequate comments (PEAR standards, class headers, etc.).

• Your class, method and variable naming is appropriate.


# Requirements


* Apache (httpd) 2.4.x with enabled php7_module & rewrite_module.

``` 
# tested on
Apache/2.4.18 
```

* Apache (httpd) 2.4.x with the following apache2.conf 

```
Mutex file:${APACHE_LOCK_DIR} default
ServerSignature Off
ServerTokens Prod
PidFile ${APACHE_PID_FILE}
Timeout 300
KeepAlive On
MaxKeepAliveRequests 100
KeepAliveTimeout 5
User ${APACHE_RUN_USER}
Group ${APACHE_RUN_GROUP}
HostnameLookups Off
ErrorLog ${APACHE_LOG_DIR}/error.log
LogLevel warn
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf
Include ports.conf
<Directory />
	Options FollowSymLinks
	AllowOverride All
	Require all denied
</Directory>
<Directory /usr/share>
	AllowOverride None
	Require all granted
</Directory>
<Directory /var/www/>
	Options Indexes FollowSymLinks
	AllowOverride All
	Require all granted
</Directory>
AccessFileName .htaccess
<FilesMatch "^\.ht">
	Require all denied
</FilesMatch>
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
IncludeOptional conf-enabled/*.conf
IncludeOptional sites-enabled/*.conf
```

* Apache (httpd) 2.4.x with the following vhost configuration for the API

```
<VirtualHost *:80>
	ServerAdmin noreply@localhost
	DocumentRoot /var/www/html
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

  	<Directory /var/www/>
    	Options Indexes FollowSymLinks MultiViews
      	AllowOverride All
      	Order deny,allow
      	Allow from all
  	</Directory>

</VirtualHost
```

* Apache (httpd) 2.4.x .htaccess file must exist in the ```DirectoryRoot```
```
# Code Reuse: .htaccess for codeignighter
# Ref: https://silicondales.com/tutorials/example-codeigniter-htaccess-mod_rewrite/
RewriteEngine On
# !IMPORTANT! Set your RewriteBase here and don't forget trailing and leading
# slashes.
# If your page resides at
# http://www.example.com/mypage/test1
# then use
# RewriteBase /mypage/test1/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?/$1 [L]
# If we don't have mod_rewrite installed, all 404's
# can be sent to index.php, and everything works as normal.
# Submitted by: ElliotHaughin
ErrorDocument 404 /index.php
```
* mysql or mariadb database server

```
# tested on
mysql  Ver 15.1 Distrib 10.0.34-MariaDB using readline 5.2
```

# Application Directory Structure

```
.
├── README.md
├── application
│   ├── cache
│   ├── config
│   ├── controllers
│   ├── core
│   ├── helpers
│   ├── hooks
│   ├── index.html
│   ├── language
│   ├── libraries
│   ├── logs
│   ├── models
│   ├── third_party
│   └── views
├── composer.json
├── dbimport
│   └── issuedb.sql
├── demo
│   ├── 1-phpmyadmin-db-complete.png
│   └── 1-phpmyadmin-db-import.png
├── index.php
├── license.txt
├── system
│   ├── core
│   ├── database
│   ├── fonts
│   ├── helpers
│   ├── index.html
│   ├── language
│   └── libraries
└── tests
    ├── Bootstrap.php
    ├── README.md
    ├── mocks
    └── phpunit.xml

24 directories, 12 files
```

## REST Library, Model and Controller

* The API relies on methods exposed by the ```application/libraries/REST_Helper.php``` Class.
* The API uses the following Model ```application/models/IssueDB_Model.php``` to interace with the database.
* The API uses the following controller for calls to Products ```application/controllers/Product_Controller.php``` 
*  The API uses the following controller for calls to Questions/Answers ```application/controllers/QA_Controller.php```

# Deployment Instructions

* Copy the extracted archive to the ```DocumentRoot``` defined in your Apache Web Server.

## Database Import

* The database schema with data is located in the ```dbimport``` directory and the file is called ```issuedb.sql```

* Importing using phpmyadmin using the Import tab
![](demo/1-phpmyadmin-db-import.png)
* Click on Go to import
![](demo/1-phpmyadmin-db-complete.png) 
* Importing using mysql command line, update host, port, etc as per your environment

```
cat dbimport/issuedb.sql | mysql -h localhost -P 3306 --protocol=tcp -uroot -pp1554n
```

* Optionally creating the database user account for the API
```
CREATE USER 'issuedb'@'localhost' IDENTIFIED BY 'p1554n';
GRANT ALL PRIVILEGES ON issuedb.* TO 'issuedb'@'%' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON issuedb.* TO 'issuedb'@'%';
FLUSH PRIVILEGES;
```
* If you change the host, user (default root) or password (default p1554n), please update the codeignighter ```application/config/database.php``` file to reflect the new details.

```
$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'p1554n',
	'database' => 'issuedb',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt'  => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
```

### Site base_url
* Update ```application/config/config.php``` defaults are

```
$config['base_url'] = 'http://'.$_SERVER['SERVER_NAME'];
```

# API


## Get Products

  Retreive a list of Products or a single Product by ID.

* **URL**

  `api/v1/products` or `api/v1/products/(:num)`

* **Method:**
  

  `GET`


*  **URL Params**
  

   **Required:**
 
   None


   **Optional:**
 
   Product number, e.g: ```api/v1/product/1```

* **Data Params**

  ```
	{
	  "error_code": "200",
	  "error_message": "operation successful",
	  "results": [
	    {
	      "id": "1",
	      "name": "Airtime & Data Bundles",
	      "description": "2G/3G/4G",
	      "created_at": "2018-08-12 12:50:38"
	    },
	    {
	      "id": "2",
	      "name": "Data & Network Coverage",
	      "description": "2G/3G/4G",
	      "created_at": "2018-08-12 12:50:38"
	    },
	    {
	      "id": "3",
	      "name": "Online Store",
	      "description": "Products purchased online.",
	      "created_at": "2018-08-12 12:50:38"
	    }
	  ]
	}
  ```

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** 
    `
	{
	  "error_code": "400",
	  "error_message": "bad request",
	  "results": "null"
	} 
	`
  * **Code:** 500 <br />
    **Content:** 
    `
	{
	  "error_code": "500",
	  "error_message": "operation error",
	  "results": "null"
	} 
	`   
	
	* This means either there is a problem with the application type (not JSON), a problem connecting to the database, there were no results returned from the database, or data parameters are missing.

* **Sample Call:**

```
 curl -sX \
 GET http://localhost/api/v1/products \
 -H 'Content-Type: application/json'
 {
  "error_code": "200",
  "error_message": "operation successful",
  "results": [
    {
      "id": "1",
      "name": "Airtime & Data Bundles",
      "description": "2G/3G/4G",
      "created_at": "2018-08-12 12:56:55"
    },
    {
      "id": "2",
      "name": "Data & Network Coverage",
      "description": "2G/3G/4G",
      "created_at": "2018-08-12 12:56:55"
    },
    {
      "id": "3",
      "name": "Online Store",
      "description": "Products purchased online.",
      "created_at": "2018-08-12 12:56:55"
    }
  ]
}
```
```
curl -sX \
GET http://localhost/api/v1/products/1  \
-H "Content-Type: application/json"
{
  "error_code": "200",
  "error_message": "operation successful",
  "results": [
    {
      "id": "1",
      "name": "Airtime & Data Bundles",
      "description": "2G/3G/4G",
      "created_at": "2018-08-12 12:44:32"
    }
  ]
}
```

* **Notes:**

  None



## Create Product

  Adds a product to the database.

* **URL**

  `api/v1/products/create`

* **Method:**
  

  `POST`


*  **URL Params**
  

   **Required:**
 
   None


   **Optional:**
 
   None

* **Data Params**

  ``` 
  {
  		"name": "product name", 
	  	"description": "product description"
  }
  ```

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** 
    `
	{
	  "error_code": "400",
	  "error_message": "bad request",
	  "results": "null"
	} 
	`
  * **Code:** 500 <br />
    **Content:** 
    `
	{
	  "error_code": "500",
	  "error_message": "operation error",
	  "results": "null"
	} 
	`   
	
	* This means either there is a problem with the application type (not JSON), a problem connecting to the database, there were no results returned from the database, or data parameters are missing.


* **Sample Call:**

```
 curl -sX \
 POST -d \
 '{ "name": "Test Product","description": "Test Product Added via API" }' \
 http://localhost/api/v1/products/create \
 -H "Content-Type: application/json"
 {
  "error_code": "200",
  "error_message": "operation successful",
  "results": "true"
}
```

* **Notes:**

  None


## Get Questions

  Retreive a list of Questions or a single question by ID.

* **URL**

  `api/v1/questions` or `api/v1/questions/(:num)`

* **Method:**
  

  `GET`


*  **URL Params**
  

   **Required:**
 
   None   


   **Optional:**
 
   None

* **Data Params**

  None

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** 
    `
	{
	  "error_code": "400",
	  "error_message": "bad request",
	  "results": "null"
	} 
	`
  * **Code:** 500 <br />
    **Content:** 
    `
	{
	  "error_code": "500",
	  "error_message": "operation error",
	  "results": "null"
	} 
	`   
	
	* This means either there is a problem with the application type (not JSON), a problem connecting to the database, there were no results returned from the database, or data parameters are missing.


* **Sample Call:**

```
curl -sX \
GET http://localhost/api/v1/questions  \
-H "Content-Type: application/json"
{
  "error_code": "200",
  "error_message": "operation successful",
  "results": [
    {
      "id": "1",
      "user_id": "3",
      "message": "How do I swap my sim card?",
      "is_active": "1",
      "created_at": "2018-08-12 12:44:32",
      "time_spent": "1",
      "product_id": "1"
    },
    {
      "id": "2",
      "user_id": "3",
      "message": "How do I unsubscribe from a service or subscribe to a service?",
      "is_active": "1",
      "created_at": "2018-08-12 12:44:32",
      "time_spent": "1",
      "product_id": "1"
    },
    {
      "id": "3",
      "user_id": "3",
      "message": "How do I upgrade my contract?",
      "is_active": "1",
      "created_at": "2018-08-12 12:44:32",
      "time_spent": "1",
      "product_id": "1"
    }
  ]
}
```
```
curl -sX \
GET http://localhost/api/v1/questions/1  \
-H "Content-Type: application/json"
{
  "error_code": "200",
  "error_message": "operation successful",
  "results": [
    {
      "id": "1",
      "user_id": "3",
      "message": "How do I swap my sim card?",
      "is_active": "1",
      "created_at": "2018-08-12 12:44:32",
      "time_spent": "1",
      "product_id": "1"
    }
  ]
}
```

* **Notes:**

  None

## Create Question

  Creates a question.

* **URL**

  `api/v1/questions/create`

* **Method:**
  

  `POST`


*  **URL Params**
  

   **Required:**
 
   None


   **Optional:**
 
   None

* **Data Params**

  ```
 { 
 		"user_id": "1","message": "test question"
 }
 ```

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** 
    `
	{
	  "error_code": "400",
	  "error_message": "bad request",
	  "results": "null"
	} 
	`
  * **Code:** 500 <br />
    **Content:** 
    `
	{
	  "error_code": "500",
	  "error_message": "operation error",
	  "results": "null"
	} 
	`   
	
	* This means either there is a problem with the application type (not JSON), a problem connecting to the database, there were no results returned from the database, or data parameters are missing.


* **Sample Call:**

```
curl -sX \
 POST -d \
 '{ "user_id": "1","message": "test question" }' \
 http://localhost/api/v1/questions/create \
 -H "Content-Type: application/json"
 {
  "error_code": "200",
  "error_message": "operation successful",
  "results": "true"
}

```

* **Notes:**

  None
  
## Get Answer

  Retrieves a single Answer by Question ID.

* **URL**

  `api/v1/answers/(:num)`

* **Method:**
  

  `GET`


*  **URL Params**
  

   **Required:**
 
   None


   **Optional:**
 
   None

* **Data Params**

  None

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** 
    `
	{
	  "error_code": "400",
	  "error_message": "bad request",
	  "results": "null"
	} 
	`
  * **Code:** 500 <br />
    **Content:** 
    `
	{
	  "error_code": "500",
	  "error_message": "operation error",
	  "results": "null"
	} 
	`   
	
	* This means either there is a problem with the application type (not JSON), a problem connecting to the database, there were no results returned from the database, or data parameters are missing.


* **Sample Call:**

```
curl -sX \
GET \
-H 'Content-Type: application/json' \
http://localhost/api/v1/answers/1
{
  "error_code": "200",
  "error_message": "operation successful",
  "results": [
    {
      "id": "1",
      "question_id": "1",
      "user_id": "1",
      "answer": "sms the word SWAP to 44773",
      "created_at": "2018-08-12 12:44:32",
      "votes": "10"
    },
    {
      "id": "2",
      "question_id": "1",
      "user_id": "2",
      "answer": "you can visit your nearest branch office, make sure to take your ID and proof of residence",
      "created_at": "2018-08-12 12:44:32",
      "votes": "0"
    }
  ]
}

```

* **Notes:**

  None
  
## Create Answer

  Creates an answer entry for a question.

* **URL**

  `api/v1/answers/create"`

* **Method:**
  

  `POST`


*  **URL Params**
  

   **Required:**
 
   None


   **Optional:**
 
   None

* **Data Params**

  ```
 { 
 		"user_id": "1","question_id": "1", "answer": "test answer"
 }
 ```

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** 
    `
	{
	  "error_code": "400",
	  "error_message": "bad request",
	  "results": "null"
	} 
	`
  * **Code:** 500 <br />
    **Content:** 
    `
	{
	  "error_code": "500",
	  "error_message": "operation error",
	  "results": "null"
	} 
	`   
	
	* This means either there is a problem with the application type (not JSON), a problem connecting to the database, there were no results returned from the database, or data parameters are missing.


* **Sample Call:**

```
 curl -sX \
 POST \
 -H "Content-Type: application/json" \
 -d \
 '{ "user_id": "1","question_id": "1", "answer": "test answer" }' \
 http://localhost/api/v1/answers/create
 {
  "error_code": "200",
  "error_message": "operation successful",
  "results": "true"
}

```

* **Notes:**

  None  

# Author


- Jayendren Maduray <jayendren.maduray@digitalskillsacademy.me>
