<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
Assignment: BSc Server Side Web Development, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/08/04
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
*/
class REST_Helper {
	// REST_Helper Method to validate that Content-Type is JSON
    public function validate_content_type($content_type) {
        // validate that Content-Type is JSON
        if ($content_type == "application/json") {
            return true;
        } else {
        	return false;
        }
    }
    // REST_Helper Method to json_decode raw input and inject into PHP $_POST array
    public function update_post_arr($result) {        
		if (!empty($result)) {
			$_POST = $result;
		} else {
			return false;
		}
    }
    // REST_Helper Method to json format associative array ($response_data) and pass $response_data["error_code"] to http_response_code method
    public function return_data($response_data) {
    	switch ($response_data["error_code"]) {
            /* Code Reuse: HTTP Response Codes
                Ref: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            */
    		case "200":
    			$error_message = "operation successful";
    			break;
    		case "400":    		
    			$error_message = "bad request";
    			break;  
    		case "500":
    			$error_message = "operation error";
    			break;    			  		
    		default:
    			$error_message = "operation returned an unknown response code";
    			break;
    	}
        $results = array(
        				'error_code'    => (string)$response_data["error_code"],
						'error_message' => (string)$error_message,
						'results'       => $response_data["results"],
        			); 
		$http_error_code = $results["error_code"];  
		/* Code Reuse: return http response code
			Ref: https://stackoverflow.com/questions/3258634/php-how-to-send-http-response-code
		*/      				             
        http_response_code($http_error_code);  
        /* Code Reuse: convert data to json
        	Ref: http://php.net/manual/en/function.json-encode.php
        */ 
        echo json_encode($results);  	
    }
}