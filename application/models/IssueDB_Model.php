<?php
/*
Assignment: BSc Server Side Web Development, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/08/04
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
*/
class IssueDB_Model extends CI_Model {

	var $question_data  = '';
	var $question_id    = '';
	var $answer_data = '';
	var $product_id   = '';

	/* Code Reuse: call the Model constructor
		Ref: https://forum.codeigniter.com/thread-59315.html
	*/
	public function __construct() {
		parent::__construct();
	}

	// IssueDB Model Method to retreive questions ordered by created_at DESC
	public function get_questions() {
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get_where('questions');
	    return $query->result_array();
	}

	// IssueDB Model Method to retreive a question by id
	public function get_question_by_id($question_id) {
		$query = $this->db->get_where('questions', array('id' => $question_id));	
		return $query->result_array();
	}

	// IssueDB Model Method to create a new question
	public function create_question($question_data) {
		$data = $question_data;
		// ensure that user_id and message keys exist in $question_data
		if ($question_data["user_id"] && $question_data["message"]) {
			$query = $this->db->insert('questions', $question_data);
			// return true if the question was added successfully
			if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}				
		} else {
			return false;
		}
	}	

	// IssueDB Model Method to retreive answers for a question_id ordered by votes DESC
	public function get_answer_by_question_id($question_id) {
		$this->db->order_by('votes', 'DESC');
		$query = $this->db->get_where('answers', array('question_id' => $question_id));	
		return $query->result_array();
	}	

	// IssueDB Model Method to add a answer in an existing question
	public function create_answer($answer_data) {
		$data = $answer_data;
		// ensure that user_id, question_id and answer keys exist in $question_data
		if ($answer_data["user_id"] && $answer_data["question_id"] && $answer_data["answer"]) {
			$query = $this->db->insert('answers', $answer_data);	
			// return true if the question was added successfully
			if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}				
		} else {
			return false;
		}			
	}

	// IssueDB Model Method to retreive products ordered by create_at DESC
	public function get_products() {
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get_where('products');
	    return $query->result_array();
	}

	// IssueDB Model Method to retreive product by ID
	public function get_product_by_id($product_id) {
		$query = $this->db->get_where('products', array('id' => $product_id));	
	    return $query->result_array();
	}	

	// IssueDB Model Method to create a new product
	public function create_product($product_data) {
		$data = $product_data;
		// ensure that name and description keys exist in $product_data
		if ($product_data["name"] && $product_data["description"]) {
			$query = $this->db->insert('products', $product_data);
			// return true if the product was added successfully
			if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}				
		} else {
			return false;
		}
	}	
}
?>