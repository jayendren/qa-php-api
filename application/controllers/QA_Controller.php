<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
Assignment: BSc Server Side Web Development, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/08/04
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
*/
class QA_Controller extends CI_Controller {
	
	// question Controller Method to retreive questions
	public function get_questions() {    
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');
		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code' => "400",
							 'results'    => "null",
							 );     
			// return json formated response
			$this->rest_helper->return_data($res_data);                                      
		} else {
			// connect to database and call the get_questions model
			$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
			$return = $this->IssueDB->get_questions();
			// validate that the database returned results and return http_response code 200 (OK)
			if ($return == true) { 
				$res_data = array(  
								'error_code' => "200",
								'results'    => $return,
								); 
				// return json formated response
				$this->rest_helper->return_data($res_data); 
			} else {
				// database did not return results, and http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code' => "500",
								'results'    => $return,
								);      
				// return json formated response								          
				$this->rest_helper->return_data($res_data); 
			}                                              
		}             
	}

	// question Controller Method to retreive question by id
	public function get_question_by_id($question_id) {   
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');
		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code' => "400",
							 'results'    => "null",
							 );     
			// return json formated response
			$this->rest_helper->return_data($res_data);            
		} else {
			// connect to database
			$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
			$return = $this->IssueDB->get_question_by_id($question_id);
			// validate that the database returned results and return http_response code 200 (OK)
			if ($return == true) { 
				$res_data = array(  
								'error_code' => "200",
								'results'    => $return,
								); 
				// return json formated response
				$this->rest_helper->return_data($res_data); 
			} else {
				// database did not return results, and http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code' => "500",
								'results'    => "null",
								); 
				// return json formated response               
				$this->rest_helper->return_data($res_data); 
			}                                              
		}              
	}

	// question Controller Method to create a new question
	public function create_question() {
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');

		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code' => "400",
							 'results'    => "null",
							 );     
			$this->rest_helper->return_data($res_data);            
		} else {
			// json_decode raw input and inject into PHP $_POST array
			$result = json_decode($this->input->raw_input_stream, true);
			$this->rest_helper->update_post_arr($result); 
			$input_data = array(  
							'user_id'    => $this->input->post('user_id'),
							'message'    => $this->input->post('message'),                                    
							'is_active'  => '1',  // active question
							'time_spent' => '0',  // set initial time spent to 0 mins

							);
			// Determine if user_id and message have been posted and update database 
			if ( $this->input->post('user_id') && $this->input->post('message') ) {
				// connect to database
				$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
				$return = $this->IssueDB->create_question($input_data);
				// validate if database update was successful and return http_response code 200 (OK)
				if ($return == true) {
					$res_data = array(  
									'error_code' => "200",
									'results'    => "true",
									);
					// return json formated response
					$this->rest_helper->return_data($res_data);                       
				} else {
					// database update failed, return http_response code 500 (Internal Server Error)
					$res_data = array(  
									'error_code' => "500",
									'results'    => "null",
									);
					// return json formated response
					$this->rest_helper->return_data($res_data);                       
				}                              
			}  else {
				// user_id and message was not posted, return http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code' => "500", 
								'results'    => "null",
								);    
				// return json formated response								            
				$this->rest_helper->return_data($res_data); 
			}           
		}
	}	

	// question Controller Method to retreive answers
	public function get_answers($question_id) {   
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');
		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code' => "400",
							 'results'    => "null",
							 );     
			// return json formated response
			$this->rest_helper->return_data($res_data);            
		} else {
			// connect to database
			$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
			$return = $this->IssueDB->get_answer_by_question_id($question_id);
			// validate that the database returned results and return http_response code 200 (OK)
			if ($return == true) { 
				$res_data = array(  
								'error_code' => "200",
								'results'    => $return,
								); 
				// return json formated response
				$this->rest_helper->return_data($res_data); 
			} else {
				// database did not return results, and http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code' => "500",
								'results'    => "null",
								); 
				// return json formated response               
				$this->rest_helper->return_data($res_data); 
			}                                              
		}              
	}

	// question Controller Method to add answers
	public function add_answer() {
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');
		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code' => "400",
							 'results'    => "null",
							 );     
			// return json formated response	
			$this->rest_helper->return_data($res_data);            
		} else {
			// json_decode raw input and inject into PHP $_POST array
			$result = json_decode($this->input->raw_input_stream, true);
			$this->rest_helper->update_post_arr($result); 
			// Determine if user_id, question_id and answer have been posted and update database 
			if ( $this->input->post('user_id') && $this->input->post('question_id') && $this->input->post('answer') ) {
				$input_data = array(  
								'user_id'    => $this->input->post('user_id'),
								'question_id'  => $this->input->post('question_id'),
								'answer'    => $this->input->post('answer'),
								);				
				// connect to database and call the create_answer model
				$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
				$return = $this->IssueDB->create_answer($input_data);
				// validate if db update was successful
				if ($return == true) {
					//echo "database returned.";
					$res_data = array(  
									'error_code' => "200",
									'results'    => "true",
									);
					// return json formated response
					$this->rest_helper->return_data($res_data);                       
				} else {
					$res_data = array(  
									'error_code' => "500",
									'results'    => "null",
									);
					// return json formated response
					$this->rest_helper->return_data($res_data);                       
				}                              
			} else {

				$res_data = array(  
								'error_code' => "500",
								'results'    => "null",
								);
				// return json formated response
				$this->rest_helper->return_data($res_data);				
			}         
		}
	}		
}