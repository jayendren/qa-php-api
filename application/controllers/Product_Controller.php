<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
Assignment: BSc Server Side Web Development, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/08/04
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
*/
class Product_Controller extends CI_Controller {	

	// Product Controller Method to retreive products
	public function get_products() {   
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');
		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code'    => "400",
							 'results'       => "null",
							 );     
			// return json formated response
			$this->rest_helper->return_data($res_data);            
		} else {
			// connect to database
			$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
			$return = $this->IssueDB->get_products();
			// validate that the database returned results and return http_response code 200 (OK)
			if ($return == true) { 
				$res_data = array(  
								'error_code'    => "200",
								'results'       => $return,
								); 
				// return json formated response
				$this->rest_helper->return_data($res_data); 
			} else {
				// database did not return results, and http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code'    => "500",
								'results'       => "null",
								); 
				// return json formated response               
				$this->rest_helper->return_data($res_data); 
			}                                              
		}              
	}	

	// Product Controller Method to retreive product by ID
	public function get_product_by_id($product_id) {   
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');
		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code'    => "400",
							 'results'       => "null",
							 );     
			// return json formated response
			$this->rest_helper->return_data($res_data);            
		} else {
			// connect to database
			$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
			$return = $this->IssueDB->get_product_by_id($product_id);
			// validate that the database returned results and return http_response code 200 (OK)
			if ($return == true) { 
				$res_data = array(  
								'error_code'    => "200",
								'results'       => $return,
								); 
				// return json formated response
				$this->rest_helper->return_data($res_data); 
			} else {
				// database did not return results, and http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code'    => "500",
								'results'       => "null",
								); 
				// return json formated response               
				$this->rest_helper->return_data($res_data); 
			}                                              
		}              
	}	

	// Product Controller Method to create a new product
	public function create_product() {
		/* Code reuse: loading a custom library
			Ref: https://www.codeigniter.com/user_guide/general/creating_libraries.html
		*/
		$this->load->library('rest_helper');

		/*
			Code Reuse: get_request_header to determine Content-Type - used to validate that Content-Type is JSON
			Ref: https://www.codeigniter.com/user_guide/libraries/input.html
		*/
		$validate_content_type = $this->rest_helper->validate_content_type(
			$this->input->get_request_header('Content-Type', TRUE)
		);
		// send http_response code 400 (Bad Request) if Content-Type is not application/json
		if (! $validate_content_type) {            
			$res_data = array(  
							 'error_code' => "400",
							 'results'    => "null",
							 );     
			$this->rest_helper->return_data($res_data);            
		} else {
			// json_decode raw input and inject into PHP $_POST array
			$result = json_decode($this->input->raw_input_stream, true);
			$this->rest_helper->update_post_arr($result); 
			$input_data = array(  
							'name'        => $this->input->post('name'),
							'description' => $this->input->post('description'),                                    
							);
			// Determine if name and description have been posted and update database 
			if ( $this->input->post('name') && $this->input->post('description') ) {
				// connect to database
				$this->load->model('IssueDB_Model', 'IssueDB', TRUE);            
				$return = $this->IssueDB->create_product($input_data);
				// validate if database update was successful and return http_response code 200 (OK)
				if ($return == true) {
					$res_data = array(  
									'error_code' => "200",
									'results'    => "true",
									);
					// return json formated response
					$this->rest_helper->return_data($res_data);                       
				} else {
					// database update failed, return http_response code 500 (Internal Server Error)
					$res_data = array(  
									'error_code' => "500",
									'results'    => "null",
									);
					// return json formated response
					$this->rest_helper->return_data($res_data);                       
				}                              
			}  else {
				// user_id and message was not posted, return http_response code 500 (Internal Server Error)
				$res_data = array(  
								'error_code' => "500", 
								'results'    => "null",
								);    
				// return json formated response								            
				$this->rest_helper->return_data($res_data); 
			}           
		}
	}	
}